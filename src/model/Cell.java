package model;

import javax.swing.JEditorPane;


public class Cell implements Cloneable {

	private int debug;
	private JEditorPane SwingZelle;
	
	public int getDebug() {
		return this.debug;
	}
	
	public void setDebug(int debug) {
		this.debug = debug;
	}
	
	public Cell(int debug) {
		this.debug = debug;
	}
	
	public Cell clone() throws CloneNotSupportedException{
		Cell c = new Cell(this.debug);
		
		return c;
	}
	
	public void setSwingZelle(JEditorPane jEditorPane) {
		// TODO Auto-generated method stub
		SwingZelle = jEditorPane;
	}
	
	public JEditorPane getSwingZelle() {
		return SwingZelle;
	}

}
