package model;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import model.Cell;
public class CellularAutomaton extends Observable implements Cloneable {
	
	private Cell[][] field; //Speicher fur altes Feld
	private Cell[][] cells;
	
	private boolean running;
	private boolean pause;
	private boolean[] checkBox;
	private int x, y;
	private int[] ruels;
	
	public CellularAutomaton(int x, int y, boolean[] checkBox, int[] ruels) {
		cells = new Cell[y][x];
		this.checkBox = checkBox; 
		this.ruels = ruels;
		
		for(int y1 = 0; y1 < cells.length; y1++) {
			for(int x1 = 0; x1 < cells[0].length; x1++) {
				
				cells[y1][x1] = new Cell(0);
			}
		}
		field = new Cell[y+2][x+2];
		
	}
	
	public void setFieldOn() {
		
		boolean wasOn;
		
		for(int y1 = 0; y1 < cells.length; y1++) {
			for(int x1 = 0; x1 < cells[0].length; x1++) {
				
				if(cells[y1][x1].getDebug() == 0) wasOn = false;
				else wasOn = true;
				
				cells[y1][x1].setDebug(1);
				
				if(!wasOn) {
					this.x = x1;
					this.y = y1;
					setChanged();
					notifyObservers("refresh");
				}
			}
		}
	}
	
	public void setFieldOff() {
		
		boolean wasOn;
		
		for(int y1 = 0; y1 < cells.length; y1++) {
			for(int x1 = 0; x1 < cells[0].length; x1++) {
				
				if(cells[y1][x1].getDebug() == 0) wasOn = false;
				else wasOn = true;
				
				cells[y1][x1].setDebug(0);
				
				if(wasOn) {
					this.x = x1;
					this.y = y1;
					setChanged();
					notifyObservers("refresh");
				}
			}
		}
	}

	public Cell[][] getCell() {
		return this.cells;
	}
	
	public int getDebug(int x, int y) {
		return this.cells[y][x].getDebug();
	}
	
	public boolean isRunning() {
		return this.running;
	}
	
	public void setRunning(boolean running) {
		this.running = running;
	}
	
	public boolean isPause() {
		return this.pause;
	}

	public int getX() {
		return this.x;
	}
	
	public int getY() {
		return this.y;
	}

	public Cell[][] getField() {
		return this.field;
	}
	
	public void setCell(int x, int y) {	
		if (cells[y][x].getDebug() == 0)	cells[y][x].setDebug(1);
		else if (cells[y][x].getDebug() == 1)	cells[y][x].setDebug(0);
		
		this.x = x;
		this.y = y;
		setChanged();
		notifyObservers("refresh");
	}
	
	public void run() throws CloneNotSupportedException {
		CopyField();
		this.x = 0;
		this.y = 0;
		
		for (int y = 0; y < cells.length; y++) {
			for (int x = 0; x < cells[0].length; x++) {
				if (cells[y][x].getDebug() == 0) {
					BirthRules(x, y);
				} else if (cells[y][x].getDebug() == 1) {
					DyingRules(x, y);
				}
			}
		}
		
	}
	
	public void BirthRules(int x, int y) {
		
		this.x = x;
		this.y = y;
		int neighborCounter = 0;
		int i = x+1;
		int j = y+1;
				
		i--;		//links
		if(field[j][i].getDebug() == 1 && checkBox[0]) neighborCounter++;

		j--;		//oben
		if(field[j][i].getDebug() == 1 && checkBox[1]) neighborCounter++;
		
		i++;		//rechts
		if(field[j][i].getDebug() == 1 && checkBox[2]) neighborCounter++;
		
		i++;		//rechts
		if(field[j][i].getDebug() == 1 && checkBox[3]) neighborCounter++;

		j++;		//unten
		if(field[j][i].getDebug() == 1 && checkBox[4]) neighborCounter++;

		j++;		//unten
		if(field[j][i].getDebug() == 1 && checkBox[5]) neighborCounter++;

		i--;		//links
		if(field[j][i].getDebug() == 1 && checkBox[6]) neighborCounter++;

		i--;		//links
		if(field[j][i].getDebug() == 1 && checkBox[7]) neighborCounter++;
		


		if(neighborCounter == ruels[0]) {
			cells[y][x].setDebug(1);		//wenn 3 oder mehr lebende Zellen gefunden
			setChanged();
			notifyObservers("refresh");
		}						//worden sind wird die Zelle neu belebt
				
		
	}
	
	/**
	 * 
	 * @param x
	 * @param y
	 */
	public void DyingRules(int x, int y) {

		this.x = x;
		this.y = y;
		int neighborCounter = 0;
		int i = x+1;
		int j = y+1;
		
		i--;		//links
		if(field[j][i].getDebug() == 1 && checkBox[0]) neighborCounter++;

		j--;		//oben
		if(field[j][i].getDebug() == 1 && checkBox[1]) neighborCounter++;
		
		i++;		//rechts
		if(field[j][i].getDebug() == 1 && checkBox[2]) neighborCounter++;
		
		i++;		//rechts
		if(field[j][i].getDebug() == 1 && checkBox[3]) neighborCounter++;

		j++;		//unten
		if(field[j][i].getDebug() == 1 && checkBox[4]) neighborCounter++;

		j++;		//unten
		if(field[j][i].getDebug() == 1 && checkBox[5]) neighborCounter++;

		i--;		//links
		if(field[j][i].getDebug() == 1 && checkBox[6]) neighborCounter++;

		i--;		//links
		if(field[j][i].getDebug() == 1 && checkBox[7]) neighborCounter++;
		

		if(neighborCounter > ruels[2] || neighborCounter < ruels[1]) {
			cells[y][x].setDebug(0);		//wenn 4 oder mehr lebende Zellen gefunden
			setChanged();				//worden sind wird die Zelle wegen
			notifyObservers("refresh");	//ueberbevolkerung getotet	
		}
		
	}
	
	public void CopyField() throws CloneNotSupportedException {
		for (int y = 0; y < cells.length; y++) {
			for (int x = 0; x < cells[0].length; x++) {
				field[y+1][x+1] = cells[y][x].clone();
			}
		}
		
		
		for(int y = 0; y < cells.length; y++) {							//erste spalte == letzte cellen spalte
			field[y+1][0] = cells[y][cells[0].length-1].clone();
		}
		
		for(int y = 0; y < cells.length; y++) {
			field[y+1][field[0].length-1] = cells[y][0].clone();		//letzte spalte == erste cellen Spalte
		}
		
		for(int x = 0; x < cells[0].length; x++) {
			field[0][x+1] = cells[cells.length-1][x].clone();			//erste Zeile == letzte Zeile
		}
		
		for(int x = 0; x < cells[0].length; x++) {
			field[field.length-1][x+1] = cells[0][x].clone();				//letzte Zeile == erste Zeile
		}
		
		field[0][0] = cells[cells.length-1][cells[0].length-1].clone();			//oben-links - unten-rechts
		field[field.length-1][field[0].length-1] = cells[0][0].clone();			//unten-rechts - oben-links
		field[0][field[0].length-1] = cells[cells.length-1][0].clone();			//oben-rechts - unten-links
		field[field.length-1][0] = cells[0][cells[0].length-1].clone();			//unten-links - oben-rechts
		
	}

	
	
}
