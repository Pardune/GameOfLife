package control;
/**
 * @author Birger Lueers
 */
/** Diese Klasse empfaengt Meldungen von den Fenstern; dient als Schnittstelle zwischen Fenstern und Controllern. 
 *  Diese Klasse hat zugriff auf Methoden der Fenster.
 *  @author	Birger Lueers
 */

import java.util.*;

import model.Cell;
import model.CellularAutomaton;

import view.SimulationWindow;
import view.StartWindow;

public class ViewController implements Observer {

	private StartWindow startWindow;
	private SimulationWindow simulationWindow;
	private CellularAutomaton automaton;
	private boolean isRunning;
	private boolean waitForDrawReady;
	

	public ViewController() {
		startWindow = new StartWindow();
		startWindow.addObserver(this);
		startWindow.toggleVisible();
	}

	/**
	 * Die Update Funktion
	 * 
	 * @param arg0	das aufrufende Objekt
	 * @param arg1	die Meldung;
	 * 				bei startWindow immer ein String
	 * 				bei simulationWindow immer ein string-array(fuer Klickposition), aber manchmal nur ein element lang (fuer meldung)
	 */
	
	@Override
	public void update(Observable arg0, Object arg1)
	{
		// abfragen fuer meldungen von Fenstern
		// startwindow
		if (arg0 == this.startWindow)
		{
			switch ((String) arg1)
			{
				case "start":
					System.out.println("switch fur start ausgefuhrt!");
					startWindow.toggleVisible();
					simulationWindow = new SimulationWindow(startWindow.getCellWidth(), startWindow.getCellHeight());
					
					automaton = new CellularAutomaton(startWindow.getCellWidth(), startWindow.getCellHeight(), startWindow.getCheckBox(), startWindow.getRules());
					automaton.addObserver(this);
					
					// baue Simulationsfenster
					simulationWindow.toggleVisible();
					startWindow.deleteObserver(this);
					simulationWindow.addObserver(this);
					break;
				default:
					System.out.println("\nunimplemented!!\n");
					break;

			}
		}
		
		// simulationsindow
		// empfaengt ein String Array, laenge muss geprfueft werden
		if (arg0 == this.simulationWindow)
		{
			String[] strs = (String[])arg1;
			if (strs.length > 1)
			{	// string-array ist (mindestens) 2-dimensional; geklickte Position liegt vor! -nicht nur eine simple Statusmeldung
				// gibt aus, an welcher stelle im Fenster geklickt wurde
				automaton.setCell(Integer.parseInt(strs[0]), Integer.parseInt(strs[1]));
				System.out.println("x: " + strs[0] + " y: " + strs[1] + " Debug: " + automaton.getDebug( Integer.parseInt(strs[0]), Integer.parseInt(strs[1])));
				//simulationWindow.updateView(Integer.parseInt(strs[0]), Integer.parseInt(strs[1]));
			} else {
				// enumeriere simple Statusmeldungen (per switch)
				switch ( strs[0] )
				{
				case "PlayOrPause":
					System.out.println("start geklickt, isPaused: " + simulationWindow.getIsPaused() );
				case "CellChanged":
					if (!simulationWindow.getIsPaused()){
						
						
						new Timer().schedule(new TimerTask() {
							public void run()  {
							  // do stuff
								System.out.println("timer tick");
								try {
									automaton.run();
								} catch (CloneNotSupportedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							}, 500, 50);
						
						
						/**try {
							automaton.run();
							System.out.println("run ausgefuhrt");
						} catch (CloneNotSupportedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}*/
					}
					break;
				case "+1Step":
					try {
						automaton.run();
					} catch (CloneNotSupportedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					break;

				case "FillOn":
					automaton.setFieldOn();
					break;
				case "FillOff":
					automaton.setFieldOff();
					break;
				default:
					System.out.println("\nunimplemented!!\n");
					break;
				}
			}
		}
		
		// Cellular automaton
		// sie soll das Feld refreshen und auf schwarz/weiss setzen je nachdem was die Farbe vorher ist.
		if (arg0 == this.automaton) {
			
			switch ((String) arg1) {
				case "refresh":
					simulationWindow.updateView(automaton.getX(), automaton.getY());			
					this.waitForDrawReady = true;
				
			}

		}
	}

}
