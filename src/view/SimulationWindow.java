package view;
/**
 * This class renders asd.
 * 
 * @author Birger Lueers
 * @version 0.3
 **/
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Observable;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import model.Cell;
import javax.swing.JCheckBox;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;

public class SimulationWindow extends Observable {

	private JFrame frame;
	private JPanel panelArray;
	private int x,y;
	private int TotalNumOfCellsHorz, TotalNumOfCellsVert;
	private Component[] CellComponentArray1D;
	private boolean isPaused;
	private JButton btnPause;
	private JButton btnOneStep;
	private JButton btnSave;
	private JButton btnLoad;
	private JButton btnRandomize;
	private JButton btnFillOn;
	private JButton btnFillOff;

	/**
	 * Create the application.
	 * @param int  
	 * @param integer2 
	 */
	public SimulationWindow(int width, int height) {
		this.TotalNumOfCellsHorz = width;
		this.TotalNumOfCellsVert = height;
		this.isPaused = true;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1037, 820);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		/**
		 * Hier werden die Zellen reingespeichert.
		 * dies ist einfacher zum returnen
		 */
		panelArray = new JPanel();
		panelArray.setLayout(null);
		panelArray.setBounds(10, 72, 1000, 700);
		frame.getContentPane().add(panelArray);
			
		/**
		 *  For-Schleife fuer JPanel aka Zellen
		 *	Wichtig! der Platzhalter ist 1000 x 700 pixel gross (B x H)
		 */ 
		
		//Vorschlag: int TotalNumOfCellsHorz = zellenInfoArray[0].length;
		//Vorschlag: int TotalNumOfCellsVert = zellenInfoArray[1].length;
		
		int height = (int) 700/TotalNumOfCellsVert;
		int width = (int) 1000/TotalNumOfCellsHorz;
		
		for ( y=0; y<TotalNumOfCellsVert; y++)
		{
			for ( x=0; x<TotalNumOfCellsHorz; x++)
			{
				Cell Zelle = new Cell(0);
				Zelle.setSwingZelle(new JEditorPane());
				
				Zelle.getSwingZelle().setForeground(Color.WHITE);
				// beachte: zellen haben relative Position 
				Zelle.getSwingZelle().setBounds((width*x), (height*y), width, height);
				Zelle.getSwingZelle().setBorder(new LineBorder(Color.GRAY));
				Zelle.getSwingZelle().setEditable(false);
				
				final Integer a = x;
				final Integer b = y;
				Zelle.getSwingZelle().addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent arg0) {
						String[] posStringArray = {a.toString(),b.toString()};
						setChanged();
						notifyObservers(posStringArray);
					}
				});
				;
				panelArray.add(Zelle.getSwingZelle());										// Zellen werden im JPanel PanelArray hinzugefuegt.
			}
		}
		
		
		btnPause = new JButton("Play");
		btnPause.setBounds(10, 11, 100, 50);
		
		btnOneStep = new JButton("+1 Step");
		btnOneStep.setBounds(120, 11, 100, 50);
		
		btnSave = new JButton("Save");
		btnSave.setBounds(230, 11, 100, 50);
		
		btnLoad = new JButton("Load");
		btnLoad.setBounds(340, 11, 100, 50);
		
		btnRandomize = new JButton("Fill Random");
		btnRandomize.setBounds(450, 11, 100, 50);
		
		btnFillOn = new JButton("Fill On");
		btnFillOn.setBounds(560, 11, 100, 50);
		
		btnFillOff = new JButton("Fill Off");
		btnFillOff.setBounds(670, 11, 100, 50);
		
		// Button ActionListeners
		
		
		btnPause.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				isPaused = !isPaused;
				if (isPaused){
					btnPause.setText("Play");
				} else {
					btnPause.setText("Pause");
				}
				setChanged();
				String[] strs = {"PlayOrPause"};
				notifyObservers(strs);
			}
		});
		
		btnOneStep.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				setChanged();
				String[] strs = {"+1Step"};
				notifyObservers(strs);
			}
		});
		btnSave.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
			}
		});
		
		btnLoad.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
			}
		});
		
		btnRandomize.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setChanged();
				String[] strs = {"randomize"};
				notifyObservers(strs);
			}
		});
		
		btnFillOn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setChanged();
				String[] strs = {"FillOn"};
				notifyObservers(strs);
			}
		});
		
		btnFillOff.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setChanged();
				String[] strs = {"FillOff"};
				notifyObservers(strs);
			}
		});

		frame.getContentPane().add(panelArray);
		frame.getContentPane().add(btnPause);
		frame.getContentPane().add(btnOneStep);
		frame.getContentPane().add(btnSave);
		frame.getContentPane().add(btnLoad);
		frame.getContentPane().add(btnRandomize);
		frame.getContentPane().add(btnFillOn);
		frame.getContentPane().add(btnFillOff);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("New check box");
		chckbxNewCheckBox.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent arg0) {
				System.out.println(arg0);
			}
		});
		chckbxNewCheckBox.setBounds(776, 11, 97, 23);
		frame.getContentPane().add(chckbxNewCheckBox);
	}
	
	/** 
	 * Zum umschalten der Sichtbarkeit
	 */
	public void toggleVisible(){
		frame.setVisible(!frame.isVisible());
	}
	
	public void updateView(int xToToggle, int yToToggle){
		CellComponentArray1D = panelArray.getComponents();		// 1-dimensionales array
		CellComponentArray1D[(xToToggle + TotalNumOfCellsHorz*yToToggle)].setBackground((CellComponentArray1D[xToToggle + TotalNumOfCellsHorz*yToToggle].getBackground() == Color.BLACK ? Color.WHITE : Color.BLACK));
		setChanged();
		String[] strs = {"CellChanged"};
		notifyObservers(strs);
	}

	public boolean getIsPaused() {
		return this.isPaused;
	}
}
